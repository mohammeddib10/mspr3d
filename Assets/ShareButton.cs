﻿using System . Collections;
using System . IO;
using UnityEngine;

public class ShareButton : MonoBehaviour
    {
    private string Share_Message;

    public void ClickShareButton ( )
        {
        Share_Message = "#MSPR3D_I2_EPSI";
        StartCoroutine ( TakeScreenshotAndShare ( ) );
        }

    private IEnumerator TakeScreenshotAndShare ( )
        {
        yield return new WaitForEndOfFrame ( );

        Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
        ss . ReadPixels ( new Rect ( 0 , 0 , Screen . width , Screen . height ) , 0 , 0 );
        ss . Apply ( );

        string filePath = Path.Combine( Application.temporaryCachePath, "shared img.png" );
        File . WriteAllBytes ( filePath , ss . EncodeToPNG ( ) );

        // To avoid memory leaks
        Destroy ( ss );

        new NativeShare ( ) . AddFile ( filePath ) . SetSubject ( "MSPR3D" ) . SetText ( Share_Message ) . Share ( );
        }
    }
