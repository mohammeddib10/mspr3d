﻿using TMPro;
using UnityEngine;
using UnityEngine . UI;

[RequireComponent ( typeof ( SalesforceClient ) )]
public class SalesforceClientAccount : MonoBehaviour
    {

    public TextMeshProUGUI inputField1;
    public TextMeshProUGUI inputField2;
    public TextMeshProUGUI inputField3;
    public Button ButtonSubmitCRM;
    public string salesforceUsername = "";
    public string salesforcePassword = "";

    public void OnClickCallCRM ( )
        {
        // Get Salesforce client component
        SalesforceClient sfdcClient = GetComponent<SalesforceClient>();

        // Init client & log in
        Coroutine<bool> loginRoutine = this.StartCoroutine<bool>(
            sfdcClient.login(salesforceUsername, salesforcePassword)
        );
        try
            {
            loginRoutine . getValue ( );
            Debug . Log ( "Salesforce login successful." );
            }
        catch (SalesforceConfigurationException e)
            {
            Debug . Log ( "Salesforce login failed due to invalid auth configuration" );
            throw e;
            }
        catch (SalesforceAuthenticationException e)
            {
            Debug . Log ( "Salesforce login failed due to invalid credentials" );
            throw e;
            }
        catch (SalesforceApiException e)
            {
            Debug . Log ( "Salesforce login failed" );
            throw e;
            }

        // Create sample case
        Account accountRecord = new Account(inputField1.text, inputField2.text, inputField3.text);
        Coroutine<Account> insertAccountRoutine = this.StartCoroutine<Account>(
            sfdcClient.insert(accountRecord)
        );
        Debug . Log ( "Account created" );
        }

    }
