﻿public class Account : SalesforceRecord
    {

    public const string BASE_QUERY = "SELECT Name, Phone, Fax FROM Account";

    public string Name { get; set; }
    public string Phone { get; set; }
    public string Fax { get; set; }

    public Account ( ) { }

    public Account ( string name , string phone , string fax )
        {
        Name = name;
        Phone = phone;
        Fax = fax;
        }

    public override string getSObjectName ( )
        {
        return "Account";
        }

    public override JSONObject toJson ( )
        {
        JSONObject record = base.toJson();
        record . Add ( "Name" , name );
        record . Add ( "Phone" , phone );
        record . Add ( "Fax" , fax );
        return record;
        }

    public override void parseFromJson ( JSONObject jsonObject )
        {
        base . parseFromJson ( jsonObject );
        Name = jsonObject . GetString ( "Name" );
        Phone = jsonObject . GetString ( "Phone" );
        Fax = jsonObject . GetString ( "Fax" );
        }
    }
